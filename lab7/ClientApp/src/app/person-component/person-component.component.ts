import { Component } from '@angular/core';
import { Service } from './person-component.service';

@Component({
  selector: 'personComponent',
  templateUrl: './person-component.component.html',
  styleUrls: ['./person-component.component.css'],
  providers: [Service]
})

export class PersonComponentComponent {
  dataSource: any;

  constructor(service: Service) { 
    this.dataSource = service.getPersons();

  }
}
