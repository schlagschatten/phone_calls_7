import { Injectable } from '@angular/core';
import * as AspNetData from "devextreme-aspnet-data-nojquery";

// Assign the URL of your actual data service to the variable below
const url:string = '/';
const dataSource:any = AspNetData.createStore({
      key: 'id', 
      loadUrl: url + 'api/Cities/Get',
      insertUrl: url + 'api/Cities/Post',
      updateUrl: url + 'api/Cities/Put',
      deleteUrl: url + 'api/Cities/Delete',
        onBeforeSend: function(method, ajaxOptions) {
          ajaxOptions.xhrFields = { withCredentials: true };
        }
      });



@Injectable()
export class Service { 
  getCities() { return dataSource; }

}
