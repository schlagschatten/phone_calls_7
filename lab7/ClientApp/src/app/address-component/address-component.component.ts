import { Component } from '@angular/core';
import { Service } from './address-component.service';

@Component({
  selector: 'AddressComponent',
  templateUrl: './address-component.component.html',
  styleUrls: ['./address-component.component.css'],
  providers: [Service]
})

export class AddressComponentComponent {
  dataSource: any;

  constructor(service: Service) { 
    this.dataSource = service.getAddresses();

  }
}
