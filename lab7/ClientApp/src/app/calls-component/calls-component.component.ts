import { Component } from '@angular/core';
import { Service } from './calls-component.service';

@Component({
  selector: 'callsComponent',
  templateUrl: './calls-component.component.html',
  styleUrls: ['./calls-component.component.css'],
  providers: [Service]
})

export class CallsComponentComponent {
  dataSource: any;

  constructor(service: Service) { 
    this.dataSource = service.getCalls();

  }
}
