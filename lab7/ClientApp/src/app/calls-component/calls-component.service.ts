import { Injectable } from '@angular/core';
import * as AspNetData from "devextreme-aspnet-data-nojquery";

// Assign the URL of your actual data service to the variable below
const url:string = '/';
const dataSource:any = AspNetData.createStore({
      key: 'id', 
      loadUrl: url + 'api/Calls/Get',
      insertUrl: url + 'api/Calls/Post',
      updateUrl: url + 'api/Calls/Put',
      deleteUrl: url + 'api/Calls/Delete',
        onBeforeSend: function(method, ajaxOptions) {
          ajaxOptions.xhrFields = { withCredentials: true };
        }
      });



@Injectable()
export class Service { 
  getCalls() { return dataSource; }

}
