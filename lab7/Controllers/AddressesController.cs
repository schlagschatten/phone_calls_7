﻿using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using lab7.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lab7.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AddressesController : Controller
    {
        #region Private Fields

        private PhoneCallsContext _context;

        #endregion Private Fields

        #region Public Constructors

        public AddressesController(PhoneCallsContext context)
        {
            _context = context;
        }

        #endregion Public Constructors

        #region Public Methods

        [HttpGet]
        public async Task<IActionResult> Get(DataSourceLoadOptions loadOptions)
        {
            var addresses = _context.Addresses.Select(i => new
            {
                i.id,
                i.city,
                i.street,
                i.house_number
            });

            // If you work with a large amount of data, consider specifying the PaginateViaPrimaryKey and PrimaryKey
            // properties. In this case, keys and data are loaded in separate queries. This can make the SQL execution
            // plan more efficient. Refer to the topic https://github.com/DevExpress/DevExtreme.AspNet.Data/issues/336.
            // loadOptions.PrimaryKey = new[] { "id" }; loadOptions.PaginateViaPrimaryKey = true;

            return Json(await DataSourceLoader.LoadAsync(addresses, loadOptions));
        }

        [HttpPost]
        public async Task<IActionResult> Post(string values)
        {
            var model = new Address();
            var valuesDict = JsonConvert.DeserializeObject<IDictionary>(values);
            PopulateModel(model, valuesDict);

            if (!TryValidateModel(model))
                return BadRequest(GetFullErrorMessage(ModelState));

            var result = _context.Addresses.Add(model);
            await _context.SaveChangesAsync();

            return Json(new { result.Entity.id });
        }

        [HttpPut]
        public async Task<IActionResult> Put(int key, string values)
        {
            var model = await _context.Addresses.FirstOrDefaultAsync(item => item.id == key);
            if (model == null)
                return StatusCode(409, "Object not found");

            var valuesDict = JsonConvert.DeserializeObject<IDictionary>(values);
            PopulateModel(model, valuesDict);

            if (!TryValidateModel(model))
                return BadRequest(GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete]
        public async Task Delete(int key)
        {
            var model = await _context.Addresses.FirstOrDefaultAsync(item => item.id == key);

            _context.Addresses.Remove(model);
            await _context.SaveChangesAsync();
        }

        #endregion Public Methods

        #region Private Methods

        private void PopulateModel(Address model, IDictionary values)
        {
            string ID = nameof(Address.id);
            string CITY = nameof(Address.city);
            string STREET = nameof(Address.street);
            string HOUSE_NUMBER = nameof(Address.house_number);

            if (values.Contains(ID))
            {
                model.id = Convert.ToInt32(values[ID]);
            }

            if (values.Contains(CITY))
            {
                model.city = Convert.ToString(values[CITY]);
            }

            if (values.Contains(STREET))
            {
                model.street = Convert.ToString(values[STREET]);
            }

            if (values.Contains(HOUSE_NUMBER))
            {
                model.house_number = Convert.ToString(values[HOUSE_NUMBER]);
            }
        }

        private string GetFullErrorMessage(ModelStateDictionary modelState)
        {
            var messages = new List<string>();

            foreach (var entry in modelState)
            {
                foreach (var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }

        #endregion Private Methods
    }
}