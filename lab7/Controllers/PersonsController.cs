﻿using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using lab7.Models;

namespace lab7.Controllers
{
    [Route("api/[controller]/[action]")]
    public class PersonsController : Controller
    {
        private PhoneCallsContext _context;

        public PersonsController(PhoneCallsContext context) {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get(DataSourceLoadOptions loadOptions) {
            var persons = _context.Persons.Select(i => new {
                i.id,
                i.first_name,
                i.second_name,
                i.last_name,
                i.phone_number
            });

            // If you work with a large amount of data, consider specifying the PaginateViaPrimaryKey and PrimaryKey properties.
            // In this case, keys and data are loaded in separate queries. This can make the SQL execution plan more efficient.
            // Refer to the topic https://github.com/DevExpress/DevExtreme.AspNet.Data/issues/336.
            // loadOptions.PrimaryKey = new[] { "id" };
            // loadOptions.PaginateViaPrimaryKey = true;

            return Json(await DataSourceLoader.LoadAsync(persons, loadOptions));
        }

        [HttpPost]
        public async Task<IActionResult> Post(string values) {
            var model = new Persons();
            var valuesDict = JsonConvert.DeserializeObject<IDictionary>(values);
            PopulateModel(model, valuesDict);

            if(!TryValidateModel(model))
                return BadRequest(GetFullErrorMessage(ModelState));

            var result = _context.Persons.Add(model);
            await _context.SaveChangesAsync();

            return Json(new { result.Entity.id });
        }

        [HttpPut]
        public async Task<IActionResult> Put(int key, string values) {
            var model = await _context.Persons.FirstOrDefaultAsync(item => item.id == key);
            if(model == null)
                return StatusCode(409, "Object not found");

            var valuesDict = JsonConvert.DeserializeObject<IDictionary>(values);
            PopulateModel(model, valuesDict);

            if(!TryValidateModel(model))
                return BadRequest(GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete]
        public async Task Delete(int key) {
            var model = await _context.Persons.FirstOrDefaultAsync(item => item.id == key);

            _context.Persons.Remove(model);
            await _context.SaveChangesAsync();
        }


        [HttpGet]
        public async Task<IActionResult> AddressesLookup(DataSourceLoadOptions loadOptions) {
            var lookup = from i in _context.Addresses
                         orderby i.city
                         select new {
                             Value = i.id,
                             Text = i.city
                         };
            return Json(await DataSourceLoader.LoadAsync(lookup, loadOptions));
        }

        private void PopulateModel(Persons model, IDictionary values) {
            string ID = nameof(Persons.id);
            string FIRST_NAME = nameof(Persons.first_name);
            string SECOND_NAME = nameof(Persons.second_name);
            string LAST_NAME = nameof(Persons.last_name);
            string PHONE_NUMBER = nameof(Persons.phone_number);

            if(values.Contains(ID)) {
                model.id = Convert.ToInt32(values[ID]);
            }

            if(values.Contains(FIRST_NAME)) {
                model.first_name = Convert.ToString(values[FIRST_NAME]);
            }

            if(values.Contains(SECOND_NAME)) {
                model.second_name = Convert.ToString(values[SECOND_NAME]);
            }

            if(values.Contains(LAST_NAME)) {
                model.last_name = Convert.ToString(values[LAST_NAME]);
            }

            if(values.Contains(PHONE_NUMBER)) {
                model.phone_number = Convert.ToString(values[PHONE_NUMBER]);
            }
        }

        private string GetFullErrorMessage(ModelStateDictionary modelState) {
            var messages = new List<string>();

            foreach(var entry in modelState) {
                foreach(var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }
    }
}