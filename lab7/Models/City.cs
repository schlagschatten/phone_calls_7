using System.Collections.Generic;

namespace lab7.Models
{
    public class City
    {
        #region Public Properties

        public int id { get; set; }
        public decimal price { get; set; }
        public string city { get; set; }
        public int code { get; set; }

        #endregion Public Properties
    }
}